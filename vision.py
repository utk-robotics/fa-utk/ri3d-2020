# Import the camera server
from cscore import CameraServer

import logging
# Import OpenCV and NumPy
import cv2
import numpy as np
from networktables import NetworkTables

log = logging.getLogger(__name__)

global vision_current_color

RGB_SCALE = 255
CMYK_SCALE = 100
RES = (320, 240)

def rgb_to_hsv(r, g, b):
    r = float(r)
    g = float(g)
    b = float(b)
    high = max(r, g, b)
    low = min(r, g, b)
    h, s, v = high, high, high

    d = high - low
    s = 0 if high == 0 else d/high

    if high == low:
        h = 0.0
    else:
        h = {
            r: (g - b) / d + (6 if g < b else 0),
            g: (b - r) / d + 2,
            b: (r - g) / d + 4,
        }[high]
        h /= 6

    return h * 255, s, v

def rgb_to_cmyk(r, g, b):
    if (r, g, b) == (0, 0, 0):
        # black
        return 0, 0, 0, CMYK_SCALE

    # rgb [0,255] -> cmy [0,1]
    c = 1 - r / RGB_SCALE
    m = 1 - g / RGB_SCALE
    y = 1 - b / RGB_SCALE

    # extract out k [0, 1]
    min_cmy = min(c, m, y)
    c = (c - min_cmy) / (1 - min_cmy)
    m = (m - min_cmy) / (1 - min_cmy)
    y = (y - min_cmy) / (1 - min_cmy)
    k = min_cmy

    # rescale to the range [0,CMYK_SCALE]
    return c * CMYK_SCALE, m * CMYK_SCALE, y * CMYK_SCALE, k * CMYK_SCALE

def main():
    global vision_current_color
    sd = NetworkTables.getTable('SmartDashboard')
    NetworkTables.initialize()
    cs = CameraServer.getInstance()
    cs.enableLogging()

    # Capture from the first USB Camera on the system
    # cam_human = cs.startAutomaticCapture(
    #     dev=0,
    #     name="humanview"
    # )
    # cam_human.setResolution(320, 240)
    # print("cam_human started")
    cam_mech = cs.startAutomaticCapture(
        path="/dev/v4l/by-path/platform-ci_hdrc.0-usb-0:1.1:1.0-video-index0",
        name="mechview"
    )
    cam_mech.setResolution(RES[0], RES[1])
    cam_mech.setBrightness(20)
    cam_mech.setExposureManual(2)
    cam_mech.setWhiteBalanceManual(5600)
    log.info("cam_mech started")

    # Get a CvSink. This will capture images from the camera
    cvSink = cs.getVideo(name="mechview")

    # (optional) Setup a CvSource. This will send images back to the Dashboard
    outputStream = cs.putVideo("postprocess", RES[0], RES[1])

    # Allocating new images is very expensive, always try to preallocate
    img = np.zeros(shape=(RES[1], RES[0], 3), dtype=np.uint8)
    crosshair = np.copy(img)

    # create crosshair
    for d1 in range(RES[1]//4, (RES[1]//4) * 3):
        crosshair[d1][RES[0]//2][1] = 255
    for d2 in range(RES[0]//4, (RES[0]//4) * 3):
        crosshair[RES[1]//2][d2][1] = 255

    while True:
        # Tell the CvSink to grab a frame from the camera and put it
        # in the source image.  If there is an error notify the output.
        time, img = cvSink.grabFrame(img)
        if time == 0:
            log.warn("Vision process failed to get image data.")
            log.debug(cvSink.getError())
            # Send the output the error.
            outputStream.notifyError(cvSink.getError())
            # skip the rest of the current iteration
            continue

        # TODO use more than one pixel in case of noise
        center_px_rgb = (
            img[RES[1]//2][RES[0]//2][2],
            img[RES[1]//2][RES[0]//2][1],
            img[RES[1]//2][RES[0]//2][0]
        )
        # log.info("Center single px RGB: {} {} {}".format(
        #     center_px_rgb[0],
        #     center_px_rgb[1],
        #     center_px_rgb[2]
        # ))
        center_px_cmyk = rgb_to_cmyk(*center_px_rgb)
        center_px_hsv = rgb_to_hsv(*center_px_rgb)
        log.info("Center single px CMYK:{:.2f} {:.2f} {:.2f} {:.1f}".format(
            *center_px_cmyk
        ))
        # log.info("Center single px HUE: {:.2f}".format(
        #     center_px_hsv[0]
        # ))

        if (center_px_cmyk[0] > 30) and (center_px_cmyk[2] > 45) and (center_px_cmyk[1] < 20):
            log.info("Color looks like GREEN! %.2f" % center_px_hsv[0])
            sd.putString("vision_color", "green")
        elif (center_px_cmyk[0] < 5) and (center_px_cmyk[2] < 40) and (center_px_cmyk[1] < 10):
            log.info("Color looks like BLUE! %.2f" % center_px_hsv[0])
            sd.putString("vision_color", "blue")
        elif (center_px_cmyk[0] < 10) and (center_px_cmyk[1] < 10) and (center_px_cmyk[2] < 70):
            log.info("Color looks like YELLOW! %.2f" % center_px_hsv[0])
            sd.putString("vision_color", "yellow")
        elif (center_px_cmyk[2] > 25) and (center_px_cmyk[0] < 10):
            log.info("Color looks like MAGENTA! %.2f" % center_px_hsv[0])
            sd.putString("vision_color", "magenta")


        # if (center_px_cmyk[0] > center_px_cmyk[1]) and (center_px_cmyk[0] > center_px_cmyk[2]):
        #     # cyan dominant
        #     if ((center_px_hsv[0]) < 120): # XXX MAGIC NUMBER
        #         log.info("Color looks like GREEN! %.2f" % center_px_hsv[0])
        #         sd.putString("vision_color", "green")
        #     else:
        #         log.info("Color looks like BLUE! %.2f" % center_px_hsv[0])
        #         sd.putString("vision_color", "blue")
        # elif (center_px_cmyk[2] > center_px_cmyk[1]) and (center_px_cmyk[2] > center_px_cmyk[0]):
        #     # yellow dominant
        #     log.info("Color looks like YELLOW! %.2f" % center_px_hsv[0])
        #     sd.putString("vision_color", "yellow")
        # elif (center_px_cmyk[1] > center_px_cmyk[2]) and (center_px_cmyk[1] > center_px_cmyk[0]):
        #     # magenta dominant
        #     log.info("Color looks like MAGENTA! %.2f" % center_px_hsv[0])
        #     sd.putString("vision_color", "magenta")

        # apply crosshair:
        np.maximum(img, crosshair, out=img)
        # (optional) send some image back to the dashboard
        outputStream.putFrame(img)
