
 - python37-robotpy-cscore
 - python3-codecs

Only for linux nerds:

 - vim
 - tmux
 - tree
 - rsync
 - netcat
 - mc
 - man
 - make
 - gzip
 - unzip
 - grep
 - gcc
