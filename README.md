# Big Orange Robotics Ri3D Team 2020

Written in python with robotpy

https://bigorangerobots.com/

# Controls

config by Garrett

## Driving (Arcade)

Left Stick Y: Forward/Back  
Right Stick X: Rotation

## Baller (intake, mover, shooter)

Toggle intake latch: `Start`  
Intake: Dpad up/down
Mover: left trigger (slow reverse with left bumper)  
Toggle shooter: `X` (set speed in dashboard when running)  

## Climber (arm, winch)

Arm down: `A`
Arm up: `B`
Winch: Right Trigger (hold `Back` to reverse)
Toggle hook latch: `Y`

# Dependencies

Python 3

robotpy [installation: https://robotpy.readthedocs.io/en/stable/install/robot.html#install-robotpy](https://robotpy.readthedocs.io/en/stable/install/robot.html#install-robotpy)

#### pip packages

 * pyfrc
 * robotpy-cscore
 * wpilib

#### packages for roborio

Use `download-opkg` and `install-opkg` for items in `robot-opkgs.md`.

## Deploying to RoboRIO

```bash
python3 robot.py deploy --skip-tests
```
