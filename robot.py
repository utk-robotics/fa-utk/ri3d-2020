#!/usr/bin/env python3

import logging
import wpilib
import wpilib.drive
import rev
from wpilib.interfaces import GenericHID
# import wpilib.SendableBuilder
Hand = GenericHID.Hand
from networktables import NetworkTables

global vision_current_color

log = logging.getLogger(__name__)

class MyRobot(wpilib.TimedRobot):
    def robotInit(self):
        NetworkTables.startClientTeam(3966)
        self.sd = NetworkTables.getTable('SmartDashboard')

        # DRIVETRAIN: 4 SPARK MAX (PWM)
        self.front_left_motor = wpilib.Spark(0)
        self.front_left_motor.setInverted(False)
        self.front_right_motor = wpilib.Spark(1)
        self.front_right_motor.setInverted(False)
        self.back_left_motor = wpilib.Spark(2)
        self.back_left_motor.setInverted(False)
        self.back_right_motor = wpilib.Spark(3)
        self.back_right_motor.setInverted(False)

        # drive motor groups
        self.left_motor_group = wpilib.SpeedControllerGroup(self.front_left_motor, self.back_left_motor)
        self.right_motor_group = wpilib.SpeedControllerGroup(self.front_right_motor, self.back_right_motor)

        self.drive = wpilib.drive.DifferentialDrive(self.left_motor_group, self.right_motor_group)

        # climber arm on CAN
        self.climber_arm = rev.CANSparkMax(8, rev.MotorType.kBrushless)
        self.climber_arm.setIdleMode(rev.IdleMode.kBrake)

        # climber winch on old spark (ratcheting one-way only)
        self.climber_winch = wpilib.Spark(4)

        # rotator moves wheel
        self.rotator_wheel = wpilib.Spark(5)

        # baller stuff on talon SR
        self.baller_intake = wpilib.Talon(6)
        self.baller_shooter = wpilib.Talon(7)
        self.baller_shooter_last_speed = 1.0
        self.baller_shooter_dash_builder = wpilib.SendableBuilder()
        self.baller_shooter_dash_builder.setTable(self.sd)
        self.baller_shooter.initSendable(self.baller_shooter_dash_builder)
        self.baller_mover = wpilib.Talon(8)
        self.baller_mover.setInverted(True)

        # Pneumatic compressor
        self.compressor = wpilib.Compressor(0)

        # pneumatic solenoids
        self.climber_hook_release = wpilib.Solenoid(7)
        self.baller_intake_latch = wpilib.Solenoid(6)

        # Controllers
        self.primary_controller = wpilib.XboxController(0)
        self.primary_joystick = wpilib.Joystick(0)

        log.debug("Launching Vision Process...")
        wpilib.CameraServer.launch('vision.py:main')


        self.timer = wpilib.Timer()
        self.time_start = self.timer.getMsClock()
        log.info("robotInit FINISHED")

    def teleopInit(self):
        self.compressor.setClosedLoopControl(True)

    def teleopPeriodic(self):
        """This function is called periodically during operator control."""
        self.drive.arcadeDrive(
            -self.primary_controller.getY(Hand.kLeft),
            self.primary_controller.getX(Hand.kRight)
        )

        # LT runs mover motor, LB reverses slow
        if self.primary_controller.getTriggerAxis(Hand.kLeft) != 0.0:
            self.baller_mover.set(self.primary_controller.getTriggerAxis(Hand.kLeft))
        else:
            if self.primary_controller.getBumper(Hand.kLeft):
                self.baller_mover.set(-0.35)
            else:
                self.baller_mover.set(0)

        # RT runs climber_winch, use Back to reverse
        if self.primary_controller.getTriggerAxis(Hand.kRight) != 0.0:
            if self.primary_controller.getBackButton():
                self.climber_winch.set(-self.primary_controller.getTriggerAxis(Hand.kRight))
            else:
                self.climber_winch.set(self.primary_controller.getTriggerAxis(Hand.kRight))
        else:
            self.climber_winch.set(0)

        # Y button: toggle baller_intake_latch for dropdown
        if self.primary_controller.getYButtonPressed():
            self.baller_intake_latch.set(
                not self.baller_intake_latch.get()
            )

        # Start button: toggle climber_hook_release
        if self.primary_controller.getStartButtonPressed():
            self.climber_hook_release.set(
                not self.climber_hook_release.get()
            )

        # X button: toggle shooter wheel on / off
        if self.primary_controller.getXButtonPressed():
            if self.baller_shooter.get() !=  0.0:
                self.baller_shooter_last_speed = self.baller_shooter.get()
                self.baller_shooter.set(0.0)
            else:
                self.baller_shooter.set(
                    self.baller_shooter_last_speed
                )

        # A button: climber_arm down
        if self.primary_controller.getAButton():
            self.climber_arm.set(-0.6)
        # B button: climber_arm up
        elif self.primary_controller.getBButton():
            self.climber_arm.set(0.6)
        else:
            self.climber_arm.set(0.0)

        # Dpad up/down: Intake
        pov = self.primary_joystick.getPOV()
        # print("POV: " + str(pov))
        if pov in [0, 45, 315]: # up
            self.baller_intake.set(-0.5)
        elif pov in [180, 135, 225]: # down
            self.baller_intake.set(0.5)
        else:
            # RB: run intake
            if self.primary_controller.getBumper(Hand.kRight):
                self.baller_intake.set(-0.5)
            else:
                self.baller_intake.set(0)

        # Dpad L/R: nothing
        # if pov == -1: # no dir
        #     self.climber_arm.set(0)
        #     # self.climber_arm_pwm.set(0)
        # elif pov in [90, 45, 135]: # right
        #     self.climber_arm.set(0.7)
        #     # self.climber_arm_pwm.set(0.4)
        # elif pov in [270, 315, 225]: # left
        #     self.climber_arm.set(-0.7)
        #     # self.climber_arm_pwm.set(-0.4)
        # else:
        #     self.climber_arm.set(0)
        #     # self.climber_arm_pwm.set(0)

        # left stick L/R rotates control wheel
        self.rotator_wheel.set(self.primary_controller.getX(Hand.kLeft))

    # def autonomousInit(self):
    #     pass

    def autonomousPeriodic(self):
        try:
            if self.sd.getString("vision_color", "nothing") != "green":
                self.rotator_wheel.set(0.3)
                log.info("ROTATE!")
            else:
                self.rotator_wheel.set(0.0)
                log.info("STOP!")
        except Exception as e:
            log.error(e)

    def disabledInit(self):
        log.warn("ROBOT disabledInit")
        self.drive.arcadeDrive(0, 0)

    def robotPeriodic(self):
        # TODO: do smartdashboard, networktables updates
        pass

    def testInit(self):
        log.warn("ROBOT testInit")

    def testPeriodic(self):
        log.warn("ROBOT testPeriodic")

if __name__ == "__main__":
    wpilib.run(MyRobot)
